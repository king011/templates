#ifndef __TEMPLATE_APP_H__
#define __TEMPLATE_APP_H__

#include <wx/app.h>

class templateApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // __TEMPLATE_APP_H__
