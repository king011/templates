#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "templateApp.h"
#include "templateMain.h"

IMPLEMENT_APP(templateApp);

bool templateApp::OnInit()
{
    templateMain* frame = new templateMain(0L, _("wxWidgets Application Template"));
#ifdef WIN32
    frame->SetIcon(wxICON(aaaa)); // To Set App Icon
#endif
    frame->Show();
    
    return true;
}
