#ifndef __TEMPLATE_MAIN_H__
#define __TEMPLATE_MAIN_H__

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include "templateApp.h"

class templateMain: public wxFrame
{
    public:
        templateMain(wxFrame *frame, const wxString& title);
        ~templateMain();
    private:
        enum
        {
            idMenuQuit = 1000,
            idMenuAbout
        };
        void OnClose(wxCloseEvent& event);
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        DECLARE_EVENT_TABLE()
};


#endif // __TEMPLATE_MAIN_H__